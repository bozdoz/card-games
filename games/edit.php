<?php

include_once $_SERVER["DOCUMENT_ROOT"] . '/helpers/private.php';

if (post('set')) {
	include_once $_SERVER["DOCUMENT_ROOT"] . '/games/addtodb.php';
	$title = 'Edit Game';
} elseif (get('set')) {
	$editpage = get('set');
	include_once $_SERVER["DOCUMENT_ROOT"] . '/find.php';
	$name = $title;
	$title = 'Edit Game';
	sess('set-edit', get('set'));
}
include_once $_SERVER["DOCUMENT_ROOT"] . '/games/create.php';
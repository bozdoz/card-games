<?php

include_once $_SERVER["DOCUMENT_ROOT"] . '/helpers/private.php';

$title = 'Your Games';

$scripts = Array('games');

$connection = new Connection();
$cardQuery = $connection->getGamesByUser(sess('user-id'));
$rows = $connection->extractQuery($cardQuery);
if ($rows) {
	$content = function () {
	?>
	<div class="row">
	<?php
		foreach (globals('rows') as $game) {
			?>
			<div class="your-game col-xs-6 col-sm-6 col-md-4 col-lg-3">
				<a 
				class="promo-card"
				href="/<?php echo $game['slug']; ?>/"
				>
				 <span class="link valign" ><?php echo $game['name']; ?>
				 </span>
				</a>
				<span 
					class="btn btn-danger btn-xs remove-game" 
					href="#" 
					data-id="<?php echo $game['id']; ?>">
					X
				</span>
				<a class="col-xs-12 col-sm-12 col-md-12 col-lg-12 btn btn-info edit-game" href="/games/edit/<?php echo $game['id']; ?>/">
					Edit Game
				</a>
			</div>
			<?php
		}
	?>
	</div>
	<?php
	};
} else {
	$content = function () {	
	?>
		<h2>You have no custom games:</h2>
		<p>
			<a href="/games/create/" class="btn btn-success">Make some!</a>
		</p>
	<?php
	};
}

include_once $_SERVER["DOCUMENT_ROOT"] . '/templates/base.php';
?>
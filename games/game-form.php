<form id="createSet" method="post" name="set" role="form">
	<?php 
	if (get('set') && isset($_SESSION['game-saved'])) {
		 ?>
		 <div class="alert alert-success" role="alert">
		 	Game Saved!
		 </div>
		<?php 
		unset($_SESSION['game-saved']);
	}
	?>
	<div class="form-group">
		<label for="title">Name of game</label>
		<input 
			id="title"
			class="form-control" 
			type="text" 
			placeholder="Name of set" 
			name="title"
			<?php block('name', 'value="','"'); ?> />
	</div>
	<div class="form-group">
		<label for="url">Game URL</label>
		<div class="input-group">
			<span class="input-group-addon">
				drinkinggame.ca/
			</span>
			<input 
				id="url"
				class="form-control" 
				type="text" 
				placeholder="URL will be generated automatically"
				name="slug"
				<?php block('slug', 'value="','"'); ?> />
		</div>
	</div>
	<div class="form-group">
		<label for="description">Description/Rules</label>
		<textarea 
			id="description"
			class="form-control" 
			name="description" 
			placeholder="Description/Rules"><?php block('description'); ?></textarea>
	</div>
	<?php
		if (globals('cards')) {
			foreach (globals('cards') as $card) {
	?>
	<div class="form-group anotherCard">
		<textarea class="form-control" placeholder="What should the card say?" name="card[]"><?php echo stripslashes($card); ?></textarea>
		<div class="remove button">X</div>
	</div>
	<?php
			}
		}
	?>
	<div class="form-group">
		<input 
			type="button" 
			class="addmore btn btn-success form-control"
			value="Add a card" />
	</div>
	<div class="form-group">
		<input type="submit" class="btn btn-primary form-control" value="<?php block('title'); ?>" name="set" />
	</div>
</form>
<div id="cloneElems" class="hidden">
	<div class="form-group anotherCard">
		<textarea class="form-control" placeholder="What should the card say?" name="card[]"></textarea>
		<div class="remove button">X</div>
	</div>
</div>
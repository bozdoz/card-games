<?php

include_once $_SERVER["DOCUMENT_ROOT"] . '/helpers/private.php';

if (post('set')) {
	include_once $_SERVER["DOCUMENT_ROOT"] . '/games/addtodb.php';
	$title = 'Edit Game';
}

$styles = Array('games');
$scripts = $styles;
$title = has('title') ? globals('title') : 'Create a Game';

$content = function () {
	include_once $_SERVER["DOCUMENT_ROOT"] . '/games/game-form.php';
};

include_once $_SERVER["DOCUMENT_ROOT"] . '/templates/base.php';
?>
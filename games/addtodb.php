<?php		

include_once $_SERVER["DOCUMENT_ROOT"] . '/helpers/private.php';
include_once $_SERVER["DOCUMENT_ROOT"] . '/helpers/functions.php';

$mysqli = connect();

//get vars
$user = $mysqli->real_escape_string(sess('user'));
$userId = $mysqli->real_escape_string(sess('user-id'));
$name = $mysqli->real_escape_string(post('title'));
$slug = $mysqli->real_escape_string(post('slug'));
$description = $mysqli->real_escape_string(post('description'));
$setId = null;

if (sess('set-edit') && strpos($_SERVER['REQUEST_URI'], 'edit')) {
	$setId = $mysqli->real_escape_string(sess('set-edit'));
	sess('set-edit', null);
}

if ($userId && $name && $slug) {
	if ($setId) {
		//wipe cards
		$connection = new Connection();
		$connection->wipeCardsFromGame($setId);
		$connection->close();
		//update 
		$sql = "
			UPDATE `cardsets` 
			SET name = '$name',
				description = '$description', 
				slug = '$slug'
			WHERE user = '$userId'
			AND id = '$setId';
		";
	} else {
		//create 
		// add to db
		// should select from cardsets, then update first
		/* $sql = "
			SELECT * 
			FROM `cardsets` 
			WHERE id = '$id'
		"; */
		
		$sql = "
			INSERT INTO `cardsets` (user, name, description, slug)
			VALUES ('$userId', '$name', '$description', '$slug');
		";
	}

	if ($res = $mysqli->query($sql)) {
		/* optionally, don't add cards */
		if (post('card')) {
			$insertId = $setId ? $setId : $mysqli->insert_id;
			$sql = "
				INSERT INTO `cards` (cardset, card)
				VALUES 
			";
			$cards = Array();
			$cardSQL = Array();
			foreach (post('card') as $card) {
				if ($card) {
					$card = $mysqli->real_escape_string($card);
					$cards[] = $card;
					$cardSQL[] = "('$insertId', '$card')";
				}
			}
			$sql .= implode(',', $cardSQL) . ';';
			/*print_r($sql);
			exit();*/
			if ($res = $mysqli->query($sql)) {
				$message = 'Game Added Successfully';
				sess('message', $message);
				$_SESSION['game-saved'] = 'well-it-did';
				header("location: $host/games/edit/$insertId/");
			} else {
				// error
				print_r($res);
				echo $sql;
				echo 'what?';
				$error = "Game Sql Error: \n"; 
				$error .= $mysqli->sqlstate;
				/*$error .= $sql;*/
				include_once $_SERVER["DOCUMENT_ROOT"] . '/templates/error.php';
				exit();
			}
		} else {
			/* no cards added */
			$message = 'Game Added Successfully';
			sess('message', $message);
			$_SESSION['game-saved'] = 'well-it-did';
			header("location: $host/games/edit/$insertId/");
		}
		
	} else {
		// error
		$error = "Game Sql Error: \n";
		$error .= $mysqli->sqlstate;
		$error .= $sql;
		include_once $_SERVER["DOCUMENT_ROOT"] . '/templates/error.php';
		exit();
	}
	
} else {
	$error = 'You must fill in the form properly';
	include_once $_SERVER["DOCUMENT_ROOT"] . '/templates/error.php';
	exit();
}

?>
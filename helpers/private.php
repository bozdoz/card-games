<?php
include_once $_SERVER["DOCUMENT_ROOT"] . '/helpers/session.php';
include_once $_SERVER["DOCUMENT_ROOT"] . '/helpers/functions.php';

if (!sess('user')) {
	sess('return_url', $current_url);
	header("location: " . $host . "/authenticate/");
}
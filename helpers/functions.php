<?php		
function connect() {
	return new mysqli("localhost", "randomcards", "randomcards", "cardgames");
}

function post($var) {
	global $_POST;
	return empty($_POST[$var]) ? NULL : $_POST[$var];
}

function get($var) {
	global $_GET;
	return empty($_GET[$var]) ? NULL : $_GET[$var];
}

function sess($var, $new = NULL) {
	global $_SESSION;
	if (isset($new)) {
		$_SESSION[$var] = $new;
	} else {
		return empty($_SESSION[$var]) ? NULL : $_SESSION[$var];
	}
}

function sess_once($var, $new = NULL) {
	global $_SESSION;
	if (isset($_SESSION[$var])) {
		$return = $_SESSION[$var];
		unset($_SESSION[$var]);
		return $return;
	}
}

function has($var) {
	return isset($GLOBALS[$var]);
}

function globals($var, $new = NULL) {
	if (isset($new)) {
		$GLOBALS[$var] = $new;
	} else {
		return empty($GLOBALS[$var]) ? NULL : $GLOBALS[$var];
	}	
}

function block($var, $prepend='', $append='') {
	if (isset($GLOBALS[$var]) && is_callable($GLOBALS[$var])) {
		$GLOBALS[$var]();
		return true;
	} else {
		$var = empty($GLOBALS[$var]) ? NULL : stripslashes($GLOBALS[$var]);
		echo empty($var) ? '' : $prepend.$var.$append;
		return isset($var);
	}
}

function hashLikeAMother($var, $key = 'Like a mother') {
	return base64_encode(hash_hmac('sha1', $var, $key, true));
}

class Connection {

	public function __construct() {
		$this->mysqli = new mysqli("localhost", "randomcards", "randomcards", "cardgames");
		$this->sqlstr = Array(
			'games' => "
				SELECT * 
				FROM `cardsets`
				WHERE %s = '%s'
				ORDER BY id DESC;
			",
			'checkusr' => "
				SELECT user 
				FROM `cardsets`
				WHERE id = %s
				LIMIT 1;
			",
			'remove' => "
				DELETE 
				FROM a, b
				USING cardsets a, cards b
				WHERE a.id = b.cardset 
				AND a.id = %s;
			",
			'wipecards' => "
				DELETE 
				FROM cards
				WHERE cardset = %s;
			",
			'recent' => "
				SELECT *
				FROM cardsets
				ORDER BY RAND()
				LIMIT %s;
			"
			); 
	}
	public function sql ($sql) {
		return $this->mysqli->query($sql);
	}
	public function extractQuery ($res) {
		$output = Array();
		while ($row = mysqli_fetch_assoc($res)) {
			$output[] = $row;
		}
		return $output;
	}
	public function getGamesByUser ($user) {
		$column = 'user';
		$sql = $this->sqlstr['games'];
		$sql = sprintf($sql, $column, $user);
		$res = $this->sql($sql);
		return $res;
	}
	public function removeGame ($game, $user) {
		$sql = $this->sqlstr['checkusr'];
		$sql = sprintf($sql, $game);
		$res = $this->sql($sql);
		$row = $this->extractQuery($res);		
		if ($row[0]['user'] == $user) {
			$sql = $this->sqlstr['remove'];
			$sql = sprintf($sql, $game, $game);
			$res = $this->sql($sql);
			return 'done';
		}
		return 'nope';
	}
	public function getMostRecentGames ($num = 10) {
		$sql = $this->sqlstr['recent'];
		$sql = sprintf($sql, $num);
		$res = $this->sql($sql);
		return $this->extractQuery($res);
	}
	public function wipeCardsFromGame ($game) {
		$sql = $this->sqlstr['wipecards'];
		$sql = sprintf($sql, $game);
		$res = $this->sql($sql);
		return 'maybe';
	}
	public function close () {
		return $this->mysqli->close();
	}

}

$host = 'http://';
if ($_SERVER["SERVER_PORT"] != "80") {
	$host .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"];
} else {
	$host .= $_SERVER["SERVER_NAME"];
}
$path = $_SERVER['REQUEST_URI'];
$current_url = $host . $path;
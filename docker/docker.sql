CREATE TABLE IF NOT EXISTS `users` (
    `id` INT NOT NULL AUTO_INCREMENT,
    `username` VARCHAR(40) NOT NULL,
    `password` VARCHAR(100) NOT NULL,
    `email` VARCHAR(255) NOT NULL,
    PRIMARY KEY ( `id` )
);

CREATE TABLE IF NOT EXISTS `user_requests` (
    `username` VARCHAR(40) NOT NULL,
    `password` VARCHAR(100) NOT NULL,
    `uniqid` VARCHAR(255) NOT NULL
);

CREATE TABLE IF NOT EXISTS `cardsets` (
    `id` INT NOT NULL AUTO_INCREMENT,
    `name` VARCHAR(50) NOT NULL,
    `description` VARCHAR(250) NOT NULL,
    `slug` VARCHAR(50) NOT NULL,
    `user` INT NOT NULL,
    PRIMARY KEY ( `id` ),
    FOREIGN KEY ( `user` ) REFERENCES `users` ( `id` )
        ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS `cards` (
    `id` INT NOT NULL AUTO_INCREMENT,
    `cardset` INT NOT NULL,
    `card` VARCHAR(255) NOT NULL,
    PRIMARY KEY ( `id` ),
    FOREIGN KEY ( `cardset` ) REFERENCES `cardsets` ( `id` )
        ON DELETE CASCADE
);
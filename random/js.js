/* random cards */
var card = document.getElementById('card');
var identifiers = document.getElementById('identifiers');
var suits = document.getElementById('suits');
function sizeUp () {
	var screenWidth = window.innerWidth,
		screenHeight = window.innerHeight,
		newWidth = (screenHeight / 1.4);

	if (newWidth > screenWidth) {
		/* shrink to width and height instead */
		card.style.width = screenWidth + 'px';
		card.style.height = screenHeight + 'px';
	} else {
		card.style.width = newWidth + 'px';
		card.style.height = (screenHeight - 20) + 'px';
	}
}
sizeUp();
window.onresize = sizeUp;

Array.prototype.shuffle = function () {
    var m = this.length, t, i;

    while (m) {
        i = Math.random() * m-- >> 0;

        t = this[m];
        this[m] = this[i];
        this[i] = t;
    }
    return this;
};

var numCards = cards.length,
	cardIndex = 0;

if (numCards) {
	cards.shuffle();
}

function showTitle () {
	/* show title */
	card.className = 'not-playing';
	/* hide last card option */
	$('#last-card').parent().addClass('hidden');
}

function reshuffle () {
	showTitle();
	/* shuffle cards */
	cards.shuffle();
	/* reset index */
	cardIndex = 0;
}

function cycleBack () {
	cardIndex = cardIndex > 1 ? cardIndex - 2 : cardIndex - 1;
	if (!cardIndex) {
		$('#last-card').parent().addClass('hidden');
		/* hack */
		card.className = 'remove-last-card';
	}
}

function changeCard (e) {
	if (e && e.type == 'keyup' && e.which == 37) {
		/* left button means go back? */
		cycleBack();
	}
	if (e == undefined || 
		e.type == 'mouseup' && e.which == 1 || 
		e.type == 'keyup' && e.which == 32 ||
		e.type == 'keyup' && e.which == 39 ||
		e.type == 'keyup' && e.which == 37 ||
		e.type == 'keyup' && e.which == 13) {
		
		if (cardIndex === numCards) {
			reshuffle();
		} else {
			suits.innerHTML = '';
			var newCard = cards[ cardIndex++ ];
			var className = newCard.color;

			if (card.className) {
				card.className = '';
			} else {
				$('#last-card').parent().removeClass('hidden');
			}

			identifiers.innerHTML = newCard.value + newCard.suit;
			identifiers.className = className;
			if (!isNaN(newCard.value)) {
				var num = Number(newCard.value);
				for (var i = 0; i < num; i ++) {
					var suit = document.createElement('div');
					suits.appendChild(suit);
					suit.className = className;
					suit.innerHTML = newCard.suit;
				}
			}
		}
	}
}

card.onmouseup = changeCard;
document.onkeyup = changeCard;

/* reshuffle */
$('#reshuffle').on('click', reshuffle);

/* show info */
$('#show-info').on('click', function () {
	showTitle();
	/* index goes back one, but not negative */
	cardIndex = cardIndex ? cardIndex - 1 : 0;
});
$('#last-card').on('click', function () {
	/* index goes back two, but not past -1 */
	cycleBack();
	changeCard();
});
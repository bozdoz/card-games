<?php
$quantifiers = Array(
	'A',
	'2',
	'3',
	'4',
	'5',
	'6',
	'7',
	'8',
	'9',
	'10',
	'J',
	'Q',
	'K'
);
$suits = Array(
	'&spades;' => 'black spades',
	'&clubs;' => 'black clubs',
	'&hearts;' => 'red hearts',
	'&diams;' => 'red diamonds'
);
$cards = Array();

foreach ($quantifiers as $num) {
	foreach ($suits as $suit => $color) {
		$cards[] =  Array(
			'color' => $color,
			'suit' => $suit,
			'value' => $num,
		);
	}
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN"
"http://www.w3.org/TR/html4/strict.dtd">
<html lang="en" class="dark">
<head>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link href="http://fonts.googleapis.com/css?family=Play|Roboto" rel="stylesheet" type="text/css">
<link href="/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="/css/style.css" rel="stylesheet" type="text/css" />
<link href="css.css" rel="stylesheet" type="text/css" />
</head>
<body>
<div id="banner">
    <div id="cardholder">
    <div id="cards">
        <div class="card"><span>♠</span>
        </div>
        <div class="card"><span>♣</span>
        </div>
        <div class="card"><span>♦</span>
        </div>
        <div class="card"><span>♥</span>
        </div>
        <div class="card"></div>
    </div>
    <h1 id="site-title">
        <a href="/" rel="home">Drinking Games</a>
    </h1>
    </div>
</div>
<div id="controls">
		<ul class="nav nav-pills nav-stacked">
			<li><a href="#" id="show-info">About</a></li>
			<li><a href="#" id="reshuffle">Reshuffle</a></li>
			<li class="hidden"><a href="#" id="last-card">Last Card</a></li>
		</ul>
	</div>
	<div id="card" class="not-playing">
		<div id="card-holder">
			<h2>Deck of Cards</h2>
			<h4 class="label label-primary">Click to Play</h4>
			<p id="card-text"></p>
			<div id="identifiers"></div>
			<div id="suits"></div>
		</div>
	</div>
<script src="jquery.js"></script>
<script>
var cards = [<?php 
$lastcard = end(array_keys($cards));
foreach ($cards as $card) {
	?>{
		color : "<?php echo $card['color']; ?>",
		value : "<?php echo $card['value']; ?>",
		suit : "<?php echo $card['suit']; ?>"
	}<?php
	if ($card != $lastcard) {
		echo ',';
	}
} 
?>
];
</script>
<script src="js.js"></script>
</body>
</html>
#!/bin/sh
set -ex

# Setup mail
if [ -n "$MAIL_HOST" ]; then
    if [ ! -f /etc/msmtprc ]; then
        echo "sendmail_path = \"/usr/bin/msmtp -C /etc/msmtprc -a default -t\"" >> /usr/local/etc/php/conf.d/sendmail.ini
    fi

    cat <<EOF > /etc/msmtprc
account default
tls on
auth on
host ${MAIL_HOST}
port 587
user ${MAIL_USER}
from ${MAIL_USER}
password ${MAIL_PASS}
EOF
    chmod 600 /etc/msmtprc
    chown www-data /etc/msmtprc
fi

# php entrypoint
# first arg is `-f` or `--some-option`
if [ "${1#-}" != "$1" ]; then
	set -- php "$@"
fi

exec "$@"

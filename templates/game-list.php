<?php 

$connection = new Connection();
$games = $connection->getMostRecentGames();

if ($games) {
	?>

	<h2>Play some games:</h2>
	<div class="panel-group" id="recent-games">
	<?php 
	foreach($games as $game) {
		?>
		<div class="panel panel-default">
		    <div class="panel-heading">
		      <h4 class="panel-title">
		        <a class="btn btn-xs btn-info" href="/<?php echo $game['slug']; ?>/">Play</a>
		        <a data-toggle="collapse" data-parent="#recent-games" href="#game-<?php echo $game['id']; ?>">
		          <?php echo $game['name']; ?>
		        </a>
		      </h4>
		    </div>
		    <div id="game-<?php echo $game['id']; ?>" class="panel-collapse collapse">
		      <div class="panel-body">
		      	<h3>Description/Rules:</h3>
		      	<div class="well">
		      	<p>
		        <?php 
		        if ($game['description']) {
		        	echo str_replace('\n', '</p><p>', $game['description']);
			    } else {
			    	echo 'None!';
			    }
		        ?>
		        </p>
		        </div>
				<a class="btn btn-primary" href="/<?php echo $game['slug']; ?>/">Play <?php echo $game['name']; ?></a>
		      </div>
		    </div>
		  </div>
		<?php
	}
	?>
	</div>
	<?php
	$extra_script = function () {
		?>
		<script src="/js/bootstrap.min.js"></script>
		<script src="/js/main.js"></script>
		<?php
	};
}
?>
<div>
	<h2><a class="btn btn-info" href="/random/">Play with a deck of cards</a></h2>
</div>
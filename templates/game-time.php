<?php 

$styles = Array('card-game');

$page = function () {
?>
	<div id="controls">
		<ul class="nav nav-pills nav-stacked">
			<li><a href="#" id="show-info">About</a></li>
			<li><a href="#" id="reshuffle">Reshuffle</a></li>
			<li class="hidden"><a href="#" id="last-card">Last Card</a></li>
		</ul>
	</div>
	<div id="card" class="not-playing">
		<div id="card-holder">
			<h2><?php block('title'); ?></h2>
			<h3>Description/Rules:</h3>
			<p id="game-description">
			<?php 
				if (block('description')) {
				} else {
					echo 'None!';
				}?>
			</p>
			<h4 class="label label-primary">Click to Play</h4>
			<p id="card-text"></p>
		</div>
	</div>
<?php
};
$extra_script = function () {
	if (has('cards')) {
		function addArraySlashes($str) {
		    return htmlspecialchars(addslashes(trim($str)));
		}
		$cards = array_map("addArraySlashes", globals('cards'));
		echo '<script>';
		echo 'var cards = ["';
		echo implode('","', $cards);
		echo '"]';
		echo '</script>';
        echo '<script src="/js/card-game.js"></script>';
	}
};

include_once $_SERVER["DOCUMENT_ROOT"] . '/templates/base.php';
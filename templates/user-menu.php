<?php
?>

<ul>
	<?php
	if (sess('user')) {
		$links = Array(
			'Games' => '/games/',
			'Create' => '/games/create/',
			'Logout' => '/logout/',
			);
	} else {
		$links = Array(
			'Login' => '/login/',
			'Sign Up' => '/register/',
			);
	}
	foreach ($links as $key => $value) {
		?>
	    <li>
	        <a 
	        class="<?php if ($path == $value) echo 'active'; ?>"
	        href="<?php echo $value; ?>">
	        <?php echo $key; ?>
	        </a>
	    </li>
		<?php 
	}
	?>
    <li class="message right">
        <?php 
        if (has('message')) {
        	echo globals('message');
        } else {
	        echo sess('user'); 
        }
        ?>
    </li>
</ul>
<?php 
include_once $_SERVER["DOCUMENT_ROOT"] . '/helpers/session.php';
include_once $_SERVER["DOCUMENT_ROOT"] . '/helpers/functions.php';
$message = sess('message');
sess('message', '');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN"
"http://www.w3.org/TR/html4/strict.dtd">
<html lang="en" class="dark">

<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta charset="UTF-8" />
    <link href='http://fonts.googleapis.com/css?family=Play|Roboto' rel='stylesheet' type='text/css'>
    <link href="/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="/css/style.css" rel="stylesheet" type="text/css" />
    <?php if (isset($styles)) { foreach ($styles as $style) { echo '<link href="/css/'.$style. '.css" rel="stylesheet" type="text/css" />'; } } ?>
    <title>
    <?php block( 'title', '', ' ' ); ?> - Drinking Games</title>
</head>

<body>
    <div id="page">
        <?php if ($message): ?>
        <div id="messages" class="messages">
            <div class="alert alert-info alert-dismissible" role="alert">
                <?php echo $message; ?>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        </div>
        <?php endif; ?>

        <?php include $_SERVER["DOCUMENT_ROOT"] . '/templates/logo.php'; ?>
        
        <?php if (block('page')) {

            } else {
        ?>
        <!-- #logo -->
        <div class="topnav main clearfix">
            <?php include $_SERVER["DOCUMENT_ROOT"] . '/templates/user-menu.php'; ?>
        </div>
        <div class="topnav secondary fixed">
            <?php include $_SERVER["DOCUMENT_ROOT"] . '/templates/user-menu.php'; ?>
        </div>
        <!-- #topnav -->
        <div id="content" class="container">
            <?php if (block('content')) {
            } else {
            ?>
            <p>
                Welcome to custom drinking games, made by you, and for you (and your friends!).  But mostly made for Jeff.
            </p>
            <p>
                I might not even know you.
            </p>
            <?php  
                include $_SERVER["DOCUMENT_ROOT"] . '/templates/game-list.php';
            } 
            ?>
        </div>
        <!-- #content -->
        <?php } ?>
    </div>
    <!-- #page -->

<script src="/js/jquery.js"></script>
<?php
if (block('extra_script')) {} else {
?>
    <script src="/js/main.js"></script>
<?php 
}
if (isset($scripts)) {
    foreach ($scripts as $script) {
        echo '<script src="/js/' . $script . '.js"></script>';
    }
}
?>
</body>

</html>
<?php

include_once $_SERVER["DOCUMENT_ROOT"] . '/helpers/session.php';
include_once $_SERVER["DOCUMENT_ROOT"] . '/helpers/functions.php';

$mysqli = connect();

$sql = "SELECT DISTINCT a.name, a.slug, a.description, b.card
		FROM (
		    SELECT * 
		    FROM cardsets
		    WHERE id = @var
		    ) a
		LEFT JOIN (
		    SELECT card
		    FROM cards
		    WHERE cardset = @var
		    ) b
		ON 1;";

if (has('editpage')) {
	$id = $mysqli->real_escape_string(globals('editpage'));
	$sql = "SET @var := $id;
	" . $sql;
} else {
	$slug = $mysqli->real_escape_string(get('slug'));
	$sql = "SELECT @var := id
		FROM cardsets
		WHERE slug = '$slug';
		" . $sql;
}
$template = $_SERVER["DOCUMENT_ROOT"] . '/templates/error.php';
if ($mysqli->multi_query($sql)) {
	$cards = Array();
	do {
		if ($res = $mysqli->store_result()) {
			/*print_r($res);
			echo '<br>';*/
			if ($res->{'num_rows'} == 0) {
				$error = 'Cannot find card game!';
			}
			while ($row = mysqli_fetch_assoc($res)) {
				/* new get cards from sql: */
				if (isset($row['name'])) {
					$title = $row['name'];
					$slug = $row['slug'];
					$description = $row['description'];
					$cards[] = $row['card'];
					$template = $_SERVER["DOCUMENT_ROOT"] . '/templates/game-time.php';
				}
			}
			$res->free();
		}
	
	} while ($mysqli->next_result());
} else {
	$error = 'Server Error!';
}
if (!has('editpage')) {
	include_once $template;
}
$("#createSet")
  .on("click", ".addmore", function () {
    var clone = $("#cloneElems").find(".anotherCard").clone();
    var addBefore = $(this).parent();
    addBefore.before(clone);
    clone.find("textarea").focus();
  })
  .on("click", ".remove", function () {
    $(this).parent().remove();
  });

(function () {
  var slug = $("#url");
  $("#title").on("keyup", function () {
    var slugval = encodeURIComponent(this.value),
      slugval = slugval.replace(/%20/g, "-"),
      slugval = slugval.toLowerCase();
    slug.val(slugval);
  });

  $(".remove-game").on("click", function () {
    var $this = $(this),
      ajaxOptions = {
        url: "/games/remove/",
        type: "POST",
        data: {
          "game-id": $this.data("id"),
        },
      },
      request = $.ajax(ajaxOptions),
      parent = $this.parent();
    parent.fadeOut();
    request.done(function (data) {
      if (data === "done") {
        parent.fadeOut(function () {
          parent.remove();
        });
      } else {
        parent.fadeIn();
        alert("failed to delete game");
      }
    });
  });
})();

$('.dropdown').on('click', function () {
	$(this).next('.submenu').slideToggle();
});

var $window = $(window),
	$topnav = $('.topnav.main'),
	$topnavfixed = $('.topnav.secondary'),
	topoffset = $topnav.offset().top;

$window.scroll(function() {
	if ($window.scrollTop() >= topoffset) {
		$topnavfixed.show();
	} else {
		$topnavfixed.hide();
	}
});

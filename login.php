<?php

include_once $_SERVER["DOCUMENT_ROOT"] . '/helpers/session.php';
include_once $_SERVER["DOCUMENT_ROOT"] . '/helpers/functions.php';

$title = 'Login';

if (post('login')) {
	// login form
	
	$mysqli = connect();
	
	//get vars
	$un = $mysqli->real_escape_string(strtolower(post('username')));
	$pass = post('password');

	if ($un && $pass) {
		// hash pass
		$pass = $mysqli->real_escape_string(hashLikeAMother($pass));
		$sql = "
			SELECT id
			FROM users
			WHERE username = '$un'
			AND password = '$pass'
			LIMIT 1;
		";

		if ($res = $mysqli->query($sql)) {
			if ($res->num_rows === 1) {
				// set session state
				sess('user', $un);
				sess('message', 'Hello');
				while ($row = mysqli_fetch_assoc($res)) {
					sess('user-id', $row['id']);
				}
				
				$redirect = post('redirect') ? post('redirect') : $host;

				header('location: ' . $redirect);
					
			} else {
				$error = 'Incorrect username or password';
			}
		}
		
	} else {
		$error = 'You must fill in the form properly';
	}
}

if (!isset($redirect)) {
	$redirect = post('redirect') ? post('redirect') : $_SERVER['HTTP_REFERER'];
}

$content = function () {
	global $redirect;
?>
<form id="login-form" role="form" method="post">
	<div class="form-group">
		<label class="sr-only" for="username">Username</label>
		<input type="text" class="form-control" id="username" placeholder="username" name="username" />
	</div>
	<div class="form-group">
		<label class="sr-only" for="loginPassword">Password</label>
		<input type="password" class="form-control" id="loginPassword" placeholder="password" name="password" />
	</div>
	<div class="form-group">
		<input type="hidden" name="redirect" value="<?php echo $redirect; ?>" />
		<button type="submit" class="btn btn-primary" name="login" value="login">Enter</button>
	</div>
</form>
<?php 
};

include_once has('error') ? 'templates/error.php' : 'templates/base.php';
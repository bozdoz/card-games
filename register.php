<?php

include_once 'helpers/session.php';
include_once 'helpers/functions.php';

$success = false;
$title = 'Signup';

if (isset($_POST['signup'])) {
	// sign up form
	
	$mysqli = connect();
	
	//get vars
	$un = $mysqli->real_escape_string(strtolower(post('username')));
	
	// checks for username
	if (strlen($un) < 2) {
		$error = 'Come on.  Make a longer username';
	} else {
		$email = $mysqli->real_escape_string(strtolower(post('email')));
		$pass = post('password');
		$passagain = post('repassword');
		
	if ($un && $email && $pass && $passagain && $pass == $passagain) {
			// add to db
			//hash pass
			$pass = $mysqli->real_escape_string(hashLikeAMother($pass));
			// uniqid for authenticating
			$uniqid = $mysqli->real_escape_string(hashLikeAMother($email));

			$sql = "
				INSERT INTO `user_requests` (username, password, uniqid)
				VALUES ('$un', '$pass', '$uniqid');
			";
			
			if (!$mysqli->query($sql)) {
				if ($mysqli->sqlstate == 23000) {
					$error = 'A user with that username or email address already exists.';
				} else {
					$error = $mysqli->sqlstate;
				}
			} else {
				// email
				$username = strtolower(post('username'));
				$to      = strtolower(post('email'));
				$subject = 'Card Site';
				$body = "Greetings $username!

To confirm your email address, please click on this link: $host/signup/$uniqid/

See you soon!
--
Drinking Game Staff";
				$headers = 'From: howaboutben@hotmail.com' . "\r\n" .
					'Reply-To: howaboutben@hotmail.com' . "\r\n" .
					'X-Mailer: PHP/' . phpversion();
				
				if (mail($to, $subject, $body, $headers)) {
					$message = 'Success! An email has been sent';
					sess('message', $message);
					$success = true;
				} else {
					echo 'no mail';
					$error = 'You must fill in the form properly';
				}

			}
		} else {
			echo 'not sure';
			$message = 'You must fill in the form properly';
		}
	}
}

$content = function () { 
	if (globals('success') && globals('message')) {
		echo "<p>" . globals('message'). "</p>";
	} else {
?>
<form id="signup-form" role="form" method="post">
	<div class="form-group">
		<label class="sr-only" for="username">Username</label>
		<input type="text" class="form-control" id="username" placeholder="username" name="username" />
	</div>
	<div class="form-group">
		<label class="sr-only" for="signupEmail">Email</label>
		<input type="email" class="form-control" id="signupEmail" placeholder="email" name="email" />
	</div>
	<div class="form-group">
		<label class="sr-only" for="signupPassword">Password</label>
		<input type="password" class="form-control" id="signupPassword" placeholder="password" name="password" />
	</div>
	<div class="form-group">
		<label class="sr-only" for="signuprePassword">Repeat Password (for your own sanity)</label>
		<input type="password" class="form-control" id="signuprePassword" placeholder="password again" name="repassword" />
	</div>
	<div class="form-group">
		<button type="submit" class="btn btn-primary" name="signup">Enter</button>
	</div>
</form>
<?php 
	}
};

include_once has('error') ? 'templates/error.php' : 'templates/base.php';
FROM php:fpm

RUN docker-php-ext-install mysqli && \
    apt-get update && \
    apt-get install -y msmtp

COPY docker-entrypoint.sh /usr/local/bin/

ENTRYPOINT [ "docker-entrypoint.sh" ]
CMD ["php-fpm"]
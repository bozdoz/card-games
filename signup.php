<?php

include 'helpers/session.php';
include 'helpers/functions.php';

if (get('id')) {
	// authenticate
	
	$mysqli = connect();
		
	// check db
	// uniqid for authenticating
	$uniqid = $mysqli->real_escape_string(get('id'));
	$sql = "
		SELECT *
		FROM user_requests
		WHERE uniqid = '$uniqid'
		LIMIT 1
	";
	
	if ($res = $mysqli->query($sql)) {
		if ($res->num_rows === 1) {
			while ($row = mysqli_fetch_assoc($res)) {
				// move user_request into users
				$pass = $row['password'];
				$un = $row['username'];
				$sql = "
					INSERT INTO `users` (email, password, username)
					VALUES ('$uniqid', '$pass', '$un');
					SELECT LAST_INSERT_ID() as id;
				";
				if ($mysqli->multi_query($sql)) {
					if ($mysqli->next_result()) {
						if ($res2 = $mysqli->store_result()) {
							while ($row2 = $res2->fetch_row()) {
								$user_id = $row2[0];
			
								// set session state
								sess('user', $un);
								sess('user-id', $user_id);
							}
							$res2->free();
						}
					}
				} else {
					printf("Error: %s\n", $mysqli->sqlstate);
					exit();
				}
			}
		}
	} else {
		// error
		if ($mysqli->sqlstate == 23000) {
			echo 'A user with that username or email address already exists.';
		} else {
			printf("Error: %s\n", $mysqli->sqlstate);
		}
		exit();
	}
}

// Send back to main page, possibly with a message in SESSION
header("location: $host");
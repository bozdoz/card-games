<?php

include 'helpers/session.php';
include 'helpers/functions.php';

if (get('id')) {
	// authenticate
	
	$mysqli = connect();
		
	// check db
	// uniqid for authenticating
	$uniqid = $mysqli->real_escape_string(get('id'));
	$sql = "
		SELECT *
		FROM user_requests
		WHERE uniqid = '$uniqid'
		LIMIT 1
	";
	
	if ($res = $mysqli->query($sql)) {
		if ($res->num_rows === 1) {
			while ($row = mysqli_fetch_assoc($res)) {
				// move user_request into users
				$pass = $row['password'];
				$un = $row['username'];
				$sql = "
					INSERT INTO `users` (email, password, username)
					VALUES ('$uniqid', '$pass', '$un');
				";
				if (!$mysqli->query($sql)) {
					printf("Error: %s\n", $mysqli->sqlstate);
					exit();
				}
			}
			// set session state
			sess('user', $un);
		}
	} else {
		// error
		if ($mysqli->sqlstate == 23000) {
			echo 'A user with that username or email address already exists.';
		} else {
			printf("Error: %s\n", $mysqli->sqlstate);
		}
		exit();
	}
}

// Send back to main page, possibly with a message in SESSION
header("location: $host");